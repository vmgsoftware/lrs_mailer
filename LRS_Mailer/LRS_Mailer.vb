﻿Imports System.Configuration
Imports System.IO

Public Class MainService
    
   
    Public MyTimer As New Timers.Timer
    ReadOnly _ServiceName as String = "LRS_Mailer"

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.

        Try
            If Not EventLog.SourceExists (_ServiceName) Then
                EventLog.CreateEventSource(_ServiceName,"Event Log")
            End If
            pc_name = UCase (Environment.MachineName)
            eveLog.Source = _ServiceName
            eveLog.WriteEntry($"{_ServiceName} started") 
            MyTimer.Interval = 60000
       
            AddHandler MyTimer.Elapsed, AddressOf ticktock
            MyTimer.Start()
        Catch ex As Exception
            'File.WriteAllText($"{System.AppDomain.CurrentDomain.BaseDirectory}\\StartError.txt",ex.ToString())
            eveLog.WriteEntry(ex.Message, EventLogEntryType.Error)
            sendError(ex)
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        eveLog.WriteEntry($"{_ServiceName} stopped") 
    End Sub


    'Async Sub ticktock
    Sub ticktock
        Dim orderdb As NpgDatabaseHelper
        Try
            If DnsInternetConnection() Then
                eveLog.WriteEntry("Checking for new orders", EventLogEntryType.Information) 

                if pc_name = "LRSPARTS" Then
                    orderdb = new NpgDatabaseHelper(ConfigurationManager.ConnectionStrings("DbContextLocal").ConnectionString.ToString) 
                Else 
                    orderdb = new NpgDatabaseHelper(ConfigurationManager.ConnectionStrings("DbContextNet").ConnectionString.ToString) 
                End If

                Dim orderData As DataTable = orderdb.ExecuteDataSet($"select order_id,client_id,order_number, shipping_method,payment_method,instructions,sub_total,shipping_total,total,date_created, date_email_sent " &
                                                                    "from  parts_v1.orders where date_email_sent is null order by order_id", "orders").Tables("orders")
                If orderData.Rows.Count> 0 Then
                    For Each row_orders As DataRow In orderData.Rows
                        Dim clientData As DataTable = orderdb.ExecuteDataSet("select client_id,contact_person,company_name,vat_number,landline,cellphone,email_address,billing_address,shipping_address  " &
                                                                             $"from parts_v1.clients  where client_id ={row_orders("client_id")}", "clients").Tables("clients")
                        If clientData.Rows.Count > 0 Then
                            For Each row_client As DataRow In clientData.Rows
                                if ConstructLRSBody(row_orders, row_client) = "Success" Then
                                    orderdb.ExecuteNonQuery("update parts_v1.orders set  date_email_sent = now() where order_id =  " & row_orders("order_id") )
                                End If
                            Next
                        End If
                    Next
                Else 
                    eveLog.WriteEntry($"no rows found", EventLogEntryType.Error) 
                End If 
                orderdb.CloseConnection 
            End If
            
           ' Await Task.Delay(millisecondsDelay := 0)
        Catch ex As Exception
            eveLog.WriteEntry(ex.Message, EventLogEntryType.Error)
            sendError(ex)
        End Try
    end Sub 


End Class
