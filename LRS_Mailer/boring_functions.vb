﻿Imports System.Configuration
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports LRS_Mailer.Utilities

Module boring_functions
    Public pc_name As String = ""
    Public raw_html As String = "<!DOCTYPE html>" & vbCrLf & " <html>" & vbCrLf & "<head>" & vbCrLf & "<title></title>" & vbCrLf & "</head>" & vbCrLf & " <body>" & vbCrLf & "<table width = ""800"" align = ""center"" cellspacing = ""0"" cellpadding = ""0"" style = ""font-family: Arial,Helvetica, sans-serif; font-size: 13px; border-collapse: collapse"" bgcolor = ""#fff"">" & vbCrLf & " <tbody> <tr> <td> " & vbCrLf & " <table width = ""100%"" style = "" border-collapse: collapse"" cellspacing = ""0"" cellpadding = ""0"" >" & vbCrLf & " <tbody> <tr>" & vbCrLf & " <td colspan = ""5"" height = ""150"" align=""center"" > <img src = ""http://lrservicecentre.vmgtest.co.za/img/logo.png""  > </td>" & vbCrLf & " </tr> " & vbCrLf & "<tr>" & vbCrLf & "<td colspan = ""5"" style = ""text-align: left"" > <p style = ""line-height:20px; font-size: 18px"" > The following order was placed on the LR Service Centre website.</p></td> " & vbCrLf & " </tr> " & vbCrLf & "</tbody></table> " & vbCrLf & "<table width = ""100%"" style = "" border-collapse: collapse"" cellspacing = ""0"" cellpadding = ""5"" >" & vbCrLf & "<tbody><tr><td style = ""font-weight: bold"">Company</td>" & vbCrLf & "<td>[Company]</td>" & vbCrLf & "<td></td>" & vbCrLf & "<td style = ""font-weight: bold"">Order Number</td>" & vbCrLf & "<td>[Order Number]</td>" & vbCrLf & "</tr>" & vbCrLf & "<tr><td width = ""24.5%"" style = ""font-weight: bold"">VAT Number</td>" & vbCrLf & "<td width = ""24.5%"">[Vat Number]</td>" & vbCrLf & "<td width = ""2%""></td>" & vbCrLf & "<td width = ""24.5%"" style = ""font-weight: bold"">Payment Method</td>" & vbCrLf & "<td width = ""24.5%"">[Payment Method]</td></tr>" & vbCrLf & "<tr><td width = ""24.5%"" style = ""font-weight: bold"">Contact</td>" & vbCrLf & "<td width = ""24.5%"">[Contact]</td>" & vbCrLf & "<td width = ""2%""></td>" & vbCrLf & "<td width = ""24.5%"" style = ""font-weight: bold"">Shipping Method</td>" & vbCrLf & "<td width = ""24.5%"">[ShippingMethod]</td></tr>" & vbCrLf & "<tr><td width = ""24.5%"" style = ""font-weight: bold"">Landline</td>" & vbCrLf & "<td width = ""24.5%"">[LandLine]</td>" & vbCrLf & "<td width = ""2%""></td>" & vbCrLf & "<td width = ""24.5%""></td>" & vbCrLf & "<td width = ""24.5%""></td></tr>" & vbCrLf & "<tr><td width = ""24.5%"" style = ""font-weight: bold"">Mobile</td>" & vbCrLf & " <td width = ""24.5%"">[Mobile]</td>" & vbCrLf & " <td width = ""2%""></td>" & vbCrLf & " <td width = ""24.5%""></td>" & vbCrLf & " <td width = ""24.5%""></td></tr>" & vbCrLf & " <tr><td width = ""24.5%"" style = ""font-weight: bold"">Email</td>" & vbCrLf & " <td width = ""24.5%"">[Email]</td>" & vbCrLf & " <td width = ""2%""></td>" & vbCrLf & "  <td width = ""24.5%""></td>" & vbCrLf & " <td width = ""24.5%""></td></tr>" & vbCrLf & " <tr><td width = ""24.5%"" style = ""font-weight: bold"" valign=""top"">Billing Address</td>" & vbCrLf & " <td width = ""24.5%"" valign=""top"">[BillingAddress]</td>" & vbCrLf & " <td width = ""2%""></td>" & vbCrLf & " <td width = ""24.5%"" style = ""font-weight: bold"" valign=""top"">Shipping Address</td>" & vbCrLf & " <td width = ""24.5%"" valign=""top"">[ShippingAddress]</td></tr>" & vbCrLf & " </tbody></table>" & vbCrLf & " <table style = ""height: 20px""><tr><td></td></tr></table>" & vbCrLf & " <table width = ""100%"" style = "" border-collapse: collapse"" cellspacing = ""0"" cellpadding = ""5"" border=""0"">" & vbCrLf & " <tbody><tr><td colspan = ""5"">" & vbCrLf & " <p style = ""line-height: 15px; font-weight: bold"">Special Instructions</p>" & vbCrLf & " <p>[Instructions]</p>" & vbCrLf & " </td></tr>" & vbCrLf & " [parts]" & vbCrLf & "</tbody></table>" & vbCrLf & "<table width = ""100%"" style = "" border-collapse: collapse"" cellspacing = ""0"" cellpadding = ""5"" border=""0""><tr><td align=""left"">[clientnotes]</td></tr>[EFTDetails]</table> " & vbCrLf & " </td></tr></tbody></table>" & vbCrLf & " </body> </html>"
    Public lrseftString As String = "<tr style=""height: 20px""><td></td></tr><tr><td align=""left""><b><u>EFT Instructions:</u></b></td></tr><tr><td align=""left"">Please use the order number as your payment reference : [OrderNo]</td></tr><tr><td><table width =""100%"" style="" border-collapse: collapse"" cellspacing=""0"" cellpadding=""5"" border=""0""><tr><td width =""24.5%"" style=""font-weight: bold"">Beneficiary</td><td width=""24.5%"">LR Service Centre</td><td width=""2%""></td><td width=""24.5%""></td><td width=""24.5%""></td></tr><tr><td width =""24.5%"" style=""font-weight: bold"">Bank</td><td width=""24.5%"">Standard Bank</td><td width=""2%""></td><td width=""24.5%""></td><td width=""24.5%""></td></tr><tr><td width =""24.5%"" style=""font-weight: bold"">Branch Code</td><td width=""24.5%"">026209</td><td width=""2%""></td><td width=""24.5%""></td><td width=""24.5%""></td></tr><tr><td width =""24.5%"" style=""font-weight: bold"">Account Number</td><td width=""24.5%"">072897295</td><td width=""2%""></td><td width=""24.5%""></td><td width=""24.5%""></td></tr><tr><td width =""24.5%"" style=""font-weight: bold"">Account Type</td><td width=""24.5%"">Current / Cheque</td><td width=""2%""></td><td width=""24.5%""></td><td width=""24.5%""></td></tr></table></td></tr><tr><td align=""left"">Please reply to this email with your proof of payment.</td></tr>"

    Public Function DnsInternetConnection() As Boolean
        Try
            Dim ipHe As IPHostEntry = Dns.GetHostEntry("www.google.com")
            Return True
        Catch
            Return False
        End Try
    End Function

    public Function ConstructLRSBody(ByVal order_row As DataRow, ByVal client_row As DataRow) As String


        Dim result As String = raw_html.Replace("[Company]", nz(client_row("company_name"),""))
        result = result.Replace("[Order Number]", order_row("order_number"))
        result = result.Replace("[Vat Number]", nz(client_row("vat_number").ToString, ""))

        If nz(order_row("shipping_method"),0) = 1 Then
            result = result.Replace("[ShippingMethod]", "Delivered to Client")
        ElseIf nz(order_row("shipping_method"),0) = 2 Then
            result = result.Replace("[ShippingMethod]", "Collecting from Epping")
        ElseIf nz(order_row("shipping_method") ,0)= 3 Then
            result = result.Replace("[ShippingMethod]", "Collecting from Montague Gardens")
        Else
            result = result.Replace("[ShippingMethod]", "")
        End If

        Dim paymentMethod As Object
        If order_row("payment_method") is nothing or IsDBNull(order_row("payment_method")) Then
            paymentMethod = -1
        End If

        If paymentMethod = 1 Then
            result = result.Replace("[Payment Method]", "CC")
        ElseIf paymentMethod = 2 Then
            result = result.Replace("[Payment Method]", "EFT")
        Else
            result = result.Replace("[Payment Method]", "")
        End If

        result = result.Replace("[Contact]", nz(client_row("contact_person").ToString, ""))
        result = result.Replace("[LandLine]", nz(client_row("landline").ToString, ""))
        result = result.Replace("[Mobile]", nz(client_row("cellphone").ToString, ""))
        result = result.Replace("[Email]", nz(client_row("email_address"), ""))
        result = result.Replace("[Instructions]", nz(order_row("instructions"), ""))
        result = result.Replace("[ShippingAddress]", nz(client_row("shipping_address"), ""))
        result = result.Replace("[BillingAddress]", nz(client_row("billing_address"), ""))

        Dim lrsemail As String = result.Replace("[parts]", doLRSParts(order_row))
        lrsemail = lrsemail.Replace("[clientnotes]", "")
        lrsemail = lrsemail.Replace("[EFTDetails]", "")

        Dim clientemail As String = result.Replace("[parts]", doClientParts(order_row))
        clientemail = clientemail.Replace("[clientnotes]", "We will contact you within two working days to confirm an estimated delivery day and time.")

        If paymentMethod = 2 Then
            clientemail = clientemail.Replace("[EFTDetails]", lrseftString.Replace("[OrderNo]", order_row("order_number")))
        Else
            clientemail = clientemail.Replace("[EFTDetails]", "")
        End If

        Try
            dim lrsMessage as MailMessage = CreateMailMessageLRS("")
            dim ClientMessage as MailMessage = CreateMailMessageLRS(nz(client_row("email_address").ToString,""))

            lrsMessage.Subject = String.Format("LRS Web Purchase - {0}", order_row("order_number"))
            lrsMessage.Body = lrsemail

            ClientMessage.Subject = String.Format("LRS Web Purchase - {0}", order_row("order_number"))
            ClientMessage.Body = clientemail

            doSendEmails().Send(lrsMessage)
            doSendEmails().Send(ClientMessage)

            ConstructLRSBody = "Success"
        Catch ex As Exception
            sendError(ex)
            ConstructLRSBody = "Error"
        End Try

    End Function

    Function CreateMailMessageLRS(email_to As string) As MailMessage
        Dim message As MailMessage = New MailMessage()
        Try
            message.From = New MailAddress(If(ConfigurationManager.AppSettings("LRSFromEmail"), "webenquiries@lrservicecentre.co.za"))

            if email_to = "" Then
                message.[To].Add("webenquiries@lrservicecentre.co.za")
                'message.Bcc.Add("stephen@vmgsoftware.co.za")
            else
                message.[To].Add(email_to)
            End If
            message.IsBodyHtml = True
        Catch ex As Exception
            sendError(ex)
            message = Nothing
        End Try
        Return message
    End Function

    Function doSendEmails() As System.Net.Mail.SmtpClient
        Dim smtpClient As System.Net.Mail.SmtpClient
        Try
            Dim userName As String = ConfigurationManager.AppSettings("LRSSmtpUser")
            Dim password As String = ConfigurationManager.AppSettings("LRSSmtpPassword")
            smtpClient = New System.Net.Mail.SmtpClient() With
                {
                .Host = If(ConfigurationManager.AppSettings("LRSSmtpHost"), "smtp.gmail.com"),
                .Port = Integer.Parse(If(ConfigurationManager.AppSettings("LRSSmtpPort"), "587")),
                .EnableSsl = Boolean.Parse(If(ConfigurationManager.AppSettings("LRSEnableSsl"), "true")),
                .UseDefaultCredentials = Boolean.Parse(If(ConfigurationManager.AppSettings("LRSUseDefaultCredentials"), "true")),
                .Credentials = New NetworkCredential(userName, password)
                }
        Catch ex As Exception
            sendError(ex)
            smtpClient = Nothing
        End Try

        Return smtpClient 

    End Function


    Function doLRSParts(ByVal order_row As DataRow) As String
        Dim parts_header As String = "<tr><th align=""left"">Qty</th><th align=""left"">Part Code</th><th align=""left"">Description</th><th align=""left"">Unit Price</th><th align=""left"">Line Total</th></tr>"
        Dim parts_footer As String = $"<tr><td width = ""10%""></td> <td width = ""20%""></td> <td width = ""40%""></td> <td width = ""15%""><b>Sub Total</b></td> <td width = ""15%"">R {order_row("sub_total").ToString()}</td></tr>  <tr><td width = ""10%""></td> <td width = ""20%""></td> <td width = ""40%""></td> <td width = ""15%""><b>Shipping</b></td> <td width = ""15%"">R {order_row("shipping_total").ToString()}</td></tr>  <tr><td width = ""10%""></td> <td width = ""20%""></td> <td width = ""40%""></td> <td width = ""15%""><b><u>Total</u></b></td> <td width = ""15%""><b><u>R {order_row("total").ToString()}</u></b></td></tr> "
        Dim parts_builder As String = ""
        Dim partsDB As NpgDatabaseHelper
        If pc_name = "LRSPARTS" Then
            partsDB = New NpgDatabaseHelper(ConfigurationManager.ConnectionStrings("DbContextLocal").ConnectionString.ToString)

        Else
            partsDB = New NpgDatabaseHelper(ConfigurationManager.ConnectionStrings("DbContextNet").ConnectionString.ToString)
        End If

        Dim partsData As DataTable = partsDB.ExecuteDataSet($"select ordered_parts.line_id,ordered_parts.order_id,ordered_parts.part_id,ordered_parts.part_desc,ordered_parts.qty,ordered_parts.price, parts.part_code, line_price " &
                    "from parts_v1.ordered_parts inner join parts_v1.parts on (parts_v1.ordered_parts.part_id = parts_v1.parts.part_id) where order_id = " & order_row("order_id") & "", "parts").Tables("parts")


        If partsData.Rows.Count > 0 Then
            For Each row_part As DataRow In partsData.Rows
                parts_builder &= $"<tr><td width = ""10%"">{row_part("qty").ToString}</td> <td width = ""20%"">{row_part("part_code").ToString}</td> <td width = ""40%"">{row_part("part_desc").ToString}</td> <td width = ""15%"">R {row_part("price").ToString}</td> <td width = ""15%"">R {row_part("line_price").ToString}</td></tr> "
            Next

        End If


        Return (parts_header & parts_builder & parts_footer)
    End Function

    Function doClientParts(ByVal order_row As DataRow) As String
        Dim parts_header As String = "<tr><th align=""left"">Qty</th><th align=""left"">Description</th><th align=""left"">Unit Price</th><th align=""left"">Line Total</th></tr>"
        Dim parts_footer As String = $"<tr><td width = ""20%""></td> <td width = ""50%""></td> <td width = ""15%""><b>Sub Total</b></td> <td width = ""15%"">R {order_row("sub_total").ToString()}</td></tr>  <tr><td width = ""20%""></td> <td width = ""50%""></td><td width = ""15%""><b>Shipping</b></td> <td width = ""15%"">R {order_row("shipping_total").ToString()}</td></tr>  <tr><td width = ""20%""></td> <td width = ""50%""></td> <td width = ""15%""><b><u>Total</u></b></td> <td width = ""15%""><b><u>R {order_row("total").ToString()}</u></b></td></tr> "
        Dim parts_builder As String = ""
        Dim partsDB As NpgDatabaseHelper
        If pc_name = "LRSPARTS" Then
            partsDB = New NpgDatabaseHelper(ConfigurationManager.ConnectionStrings("DbContextLocal").ConnectionString.ToString)
        Else
            partsDB = New NpgDatabaseHelper(ConfigurationManager.ConnectionStrings("DbContextNet").ConnectionString.ToString)
        End If

        Dim partsData As DataTable = partsDB.ExecuteDataSet($"select ordered_parts.line_id,ordered_parts.order_id,ordered_parts.part_id,ordered_parts.part_desc,ordered_parts.qty,ordered_parts.price, parts.part_code, line_price " &
                    "from parts_v1.ordered_parts inner join parts_v1.parts on (parts_v1.ordered_parts.part_id = parts_v1.parts.part_id) where order_id = " & order_row("order_id") & "", "parts").Tables("parts")

        If partsData.Rows.Count > 0 Then
            For Each row_part As DataRow In partsData.Rows
                parts_builder &= $"<tr><td width = ""20%"">{row_part("qty").ToString}</td><td width = ""50%"">{row_part("part_desc").ToString}</td> <td width = ""15%"">R {row_part("price").ToString}</td> <td width = ""15%"">R {row_part("line_price").ToString}</td></tr> "
            Next

        End If


        Return (parts_header & parts_builder & parts_footer)
    End Function

    Function nz(field As Object, ifNullValue As Object) As Object
        If IsNothing(field) Orelse IsDBNull(field) Then
            Return ifNullValue
        Else
            Return field
        End If
    End Function

    Sub sendError(ex As Exception)
        Dim stkTrace As String = ex.StackTrace
        Dim excMessage As String = ex.Message
        Dim mail As New MailMessage
        mail.To.Add("stephen@vmgsoftware.co.za")
        mail.CC.Clear()
        mail.Subject = " LRS Mailer Error"
        mail.IsBodyHtml = True

        mail.Body = "<h3>Error on the LRS Mailer</h3><table><tbody><tr><td>Title : </td><td>LRS Mailer Error</td></tr>" &
                    "<tr><td>Message : </td><td>send_notification</td></tr><tr><td>System Message:</td><td>" & excMessage & "</td></tr>" &
                    "<tr><td>Stack Trace:</td><td>" & stkTrace & "</td></tr><tr><td>Date/Time:</td><td>" & DateTime.Now.ToString() & "</td></tr></tbody></table>"

        EmailService.SendMail(mail)

        File.WriteAllText($"C:\temp\error.txt",ex.ToString())
    End Sub
End Module
